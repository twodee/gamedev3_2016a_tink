﻿using UnityEngine;
using System.Collections;

public class SpawnerController : MonoBehaviour {
  public GameObject prefab;

  void Start() {
    StartCoroutine(Emit());  
  }

  IEnumerator Emit() {
    while (true) {
      Vector2 random = 20 * Random.insideUnitCircle;
      Vector3 position = transform.position + new Vector3(random.x, 0, random.y);
      Instantiate(prefab, position, Quaternion.identity);
      yield return new WaitForSeconds(5.0f);
    }
  }
}
