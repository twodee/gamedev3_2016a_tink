﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
  public float speed;

  private new Rigidbody rigidbody;

  void Start() {
    rigidbody = GetComponent<Rigidbody>(); 
  }

  void FixedUpdate() {
    float v = Input.GetAxis("Vertical");
    rigidbody.velocity = Vector3.up * rigidbody.velocity.y + new Vector3(transform.forward.x, 0.0f, transform.forward.z) * v * speed;

    float torque = Input.GetAxis("Horizontal");
    rigidbody.AddTorque(new Vector3(0, torque * 10000, 0));
  }
}
