﻿using UnityEngine;
using System.Collections;

public class ShotController : MonoBehaviour {
  public Material[] materials = new Material[3];
  public AudioClip hitClip;
  public AudioClip pickupClip;

  private Team _team;
  public Team team {
    get {
      return _team;
    }

    set {
      if (value != _team) {
        _team = value;
        UpdateColor();

        if (_team == Team.NEUTRAL) {
          if (pendingDeath == null) {
            pendingDeath = StartCoroutine(DieAfter(20.0f));
          }
        } else {
          if (pendingDeath != null) {
            StopCoroutine(pendingDeath);
            pendingDeath = null;
          }
          gameObject.SetActive(false);
        }
      }
    }
  }

  private Coroutine pendingDeath;

  void Start() {
    team = Team.NEUTRAL;
  }

  void UpdateColor() {
    MeshRenderer renderer = GetComponent<MeshRenderer>();
    if (team == Team.A) {
      renderer.material = materials[0];
    } else if (team == Team.B) {
      renderer.material = materials[1];
    } else {
      renderer.material = materials[2];
    }
  }

  IEnumerator DieAfter(float time) {
    yield return new WaitForSeconds(time);
    Destroy(gameObject);
  }

  void OnCollisionEnter(Collision collision) {
    if (collision.gameObject.tag == "Tank") {
      TankController tank = collision.gameObject.GetComponent<TankController>();
      if (team == Team.NEUTRAL) {
        AudioSource.PlayClipAtPoint(pickupClip, transform.position);
        tank.shot = this.gameObject;
      } else if (team != tank.team) {
        tank.Flip();
        Destroy(gameObject);
      } else {
        AudioSource.PlayClipAtPoint(hitClip, transform.position);
      }
    } else {
      team = Team.NEUTRAL;
    }
  }
}
