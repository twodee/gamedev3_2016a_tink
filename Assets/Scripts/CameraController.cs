﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CameraController : MonoBehaviour {
  public Transform target;
  public float distanceY;
  public float distanceZ;
  public Text message;

  private Coroutine lockOntoInProgress;

  void Start() {
    lockOntoInProgress = null;
  }
  
  void Update() {
    if (lockOntoInProgress == null && Input.GetButtonDown("Fire1")) {
      Ray ray = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
      RaycastHit hit;
      if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Tank"))) {
        TankController tankController = hit.collider.gameObject.GetComponent<TankController>();
        if (tankController.team == Team.A) {
          lockOntoInProgress = StartCoroutine(LockOnto(hit.collider.gameObject.transform));
        }
      }

      // If I didn't click on one of my own, let's assume the player meant to
      // fire shot.
      if (lockOntoInProgress == null) {
        target.GetComponent<TankController>().Fire();
      }
    }

    if (lockOntoInProgress == null) {
      transform.position = target.transform.position - target.transform.forward * distanceZ + Vector3.up * distanceY;
      transform.LookAt(target);
    }
  }

  IEnumerator LockOnto(Transform newTarget) {
    // Release new target from AI control, because we're taking over.
    newTarget.GetComponent<NavMeshAgent>().enabled = false;

    Vector3 startPosition = transform.position;
    Vector3 endPosition = newTarget.transform.position - newTarget.transform.forward * distanceZ + Vector3.up * distanceY;

    float startTime = Time.time;
    float targetTime = 1.0f;
    float elapsedTime = 0.0f;

    do {
      elapsedTime = Time.time - startTime;
      float proportion = Mathf.Clamp01(elapsedTime / targetTime);
      transform.position = Vector3.Lerp(startPosition, endPosition, proportion);
      yield return null;
    } while (elapsedTime < targetTime);

    Quaternion startRotation = transform.rotation;
    Quaternion endRotation = Quaternion.LookRotation(newTarget.position - transform.position);

    startTime = Time.time;
    targetTime = 1.0f;
    elapsedTime = 0.0f;

    do {
      elapsedTime = Time.time - startTime;
      float proportion = Mathf.Clamp01(elapsedTime / targetTime);
      transform.rotation = Quaternion.Lerp(startRotation, endRotation, proportion);
      yield return null;
    } while (elapsedTime < targetTime);

    target.GetComponent<PlayerController>().enabled = false;
    newTarget.GetComponent<PlayerController>().enabled = true;

    target = newTarget;
    lockOntoInProgress = null;
  }

  public void LockOntoNearest() {
    // Cancel any locking onto in progress.
    if (lockOntoInProgress != null) {
      StopCoroutine(lockOntoInProgress);
      lockOntoInProgress = null;
    }

    Transform nearestTank = null;

    // TODO: find nearest tank.

    if (nearestTank != null) {
      lockOntoInProgress = StartCoroutine(LockOnto(nearestTank));
    }
  }
}
