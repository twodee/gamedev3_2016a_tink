﻿using UnityEngine;
using System.Collections;

public class TankController : MonoBehaviour {
  public Material[] materials = new Material[2];
  public Team team;

  public AudioClip flipClip;
  public AudioClip fireClip;
  
  private GameObject halo;
  private GameObject _shot;
  private new Rigidbody rigidbody;
  private AudioSource audioSource;

  public GameObject shot {
    get {
      return _shot;
    }
    set {
      if (_shot == null) {
        _shot = value;
        ShotController shotController = _shot.GetComponent<ShotController>();
        shotController.team = team;
        halo.SetActive(true);
      } else if (value == null) {
        _shot = null;
        halo.SetActive(false);
      }
    }
  }

  void Start() {
    UpdateColor();
    halo = transform.Find("Halo").gameObject; 
    rigidbody = GetComponent<Rigidbody>(); 
    audioSource = GetComponent<AudioSource>(); 
  }

  public void Flip() {
    audioSource.PlayOneShot(flipClip);

    team = team == Team.A ? Team.B : Team.A;
    UpdateColor();
    if (shot != null) {
      shot.GetComponent<ShotController>().team = team;
    }

    if (team == Team.B && GetComponent<PlayerController>().enabled) {
      Camera.main.GetComponent<CameraController>().LockOntoNearest();
    }

    // TODO: Check for win!
  }

  void Update() {
  }

  void FixedUpdate() {
    // Balance the tank if it's about to tip over.
    float angle = Vector3.Angle(Vector3.up, transform.up);
    if (angle > 45.0f) {
      rigidbody.MoveRotation(Quaternion.LookRotation(new Vector3(transform.forward.x, 0, transform.forward.z), Vector3.up));
    }
  }

  void UpdateColor() {
    Transform child = transform.Find("Tank");
    child.GetComponent<MeshRenderer>().material = materials[team == Team.A ? 0 : 1];
  }

  public void Fire() {
    if (shot != null) {
      audioSource.PlayOneShot(fireClip);
      shot.SetActive(true);
      shot.transform.position = transform.position + 4 * transform.forward;
      shot.GetComponent<Rigidbody>().AddForce(transform.forward * 3000);
      shot = null;
    }
  }
}
